# Git-Prove
I File con estensione .md vengono usati solitamente per fare delle descrizioni su un progetto
(md sta per MarkDown ed è un linguaggio di markup usato per creare qualcosa di grafico grazie ad alcune regole)

Come ad esempio questo:

## Repository usata per fare delle prove con i comandi di Git

- file Excel: contenente la descrizione di molti comandi
- file PDF: contenente la descrizione di alcuni comandi (in inglese)

Creato da: *Alessio Torricelli*, **[Linguaggi](https://alessiotorricelli.altervista.org)**